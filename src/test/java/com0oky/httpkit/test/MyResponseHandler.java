package com0oky.httpkit.test;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;

/**
 * @author mdc
 * @date 2017年6月11日
 */
public class MyResponseHandler implements ResponseHandler<String> {

	@Override
	public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
		//ResponseWrap wrap = new ResponseWrap(response);
		//注意:getEntity是不可重复读取的对象,如果需要重复读取,请使用ResponseWrap包装器
		return EntityUtils.toString(response.getEntity());
	}

}
